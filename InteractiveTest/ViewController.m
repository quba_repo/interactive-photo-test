//
//  ViewController.m
//  InteractiveTest
//
//  Created by Антон Кудряшов on 03/08/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import "ViewController.h"

#import "ContextHelper.h"
#import "UIViewController+IDPhotoAnimator.h"

@interface ViewController ()

@end

@implementation ViewController



- (IBAction)presentNext:(id)sender
{
    UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"second"];
    [self.parentViewController presentViewController:vc
                                            withView:[ContextHelper shared].view
                                         toViewFrame:[ContextHelper shared].destanationFrame
                                            animated:YES
                                          completion:nil];
}

@end
