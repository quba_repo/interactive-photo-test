//
//  ContextHelper.m
//  InteractiveTest
//
//  Created by Антон Кудряшов on 03/08/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import "ContextHelper.h"

@implementation ContextHelper

+ (instancetype)shared
{
    static ContextHelper *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        UIImageView* iv = [UIImageView new];
        iv.image = [UIImage imageNamed:@"Image"];
        iv.frame = (CGRect){0, 20, 100, 100};
        _destanationFrame = (CGRect){0, 20, 320, 320};
        _initialFrame = iv.frame;
        _view = iv;
    }
    return self;
}

@end
