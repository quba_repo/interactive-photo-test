//
//  ContextHelper.h
//  InteractiveTest
//
//  Created by Антон Кудряшов on 03/08/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContextHelper : NSObject

@property (nonatomic, strong) UIView* view;

@property (nonatomic) CGRect initialFrame;
@property (nonatomic) CGRect destanationFrame;

+ (instancetype)shared;

@end
