//
//  Destanatonvc.m
//  InteractiveTest
//
//  Created by Антон Кудряшов on 03/08/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import "Destanatonvc.h"

#import "ContextHelper.h"
#import "UIViewController+IDPhotoAnimator.h"

@interface Destanatonvc ()

@property (nonatomic, weak) IBOutlet UIPanGestureRecognizer* pan;

@end

@implementation Destanatonvc

- (IBAction)disMiss:(id)sender
{
    [ContextHelper shared].view.frame = [ContextHelper shared].destanationFrame;
    [self dismissViewControllerAnimated:YES
                               withView:[ContextHelper shared].view
                            toViewFrame:[ContextHelper shared].initialFrame
                             completion:nil];
}

- (UIPanGestureRecognizer *)interactivePanGestureForDismiss
{
    return self.pan;
}

- (IBAction)panHadnle:(UIPanGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateBegan) {
        [self disMiss:nil];
    }
}

@end
