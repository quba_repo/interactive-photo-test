//
//  main.m
//  InteractiveTest
//
//  Created by Антон Кудряшов on 03/08/16.
//  Copyright © 2016 Антон Кудряшов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
